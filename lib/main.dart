import 'dart:async';
import 'package:flutter/material.dart';
import 'package:dart_pusher_channels/dart_pusher_channels.dart';

void main() => runApp(MyApp());

connect() async {
  // Enable or disable logs
  PusherChannelsPackageLogger.enableLogs();
  const hostOptions = PusherChannelsOptions.fromHost(
    scheme: 'http', // wss for ssl
    host: '13.36.119.0',
    key: 'app-key', // default is app-key, change in production!
    shouldSupplyMetadataQueries: true,
    metadata: PusherChannelsOptionsMetadata.byDefault(),
    port: 80,
  );

  // Create an instance of PusherChannelsClient
  final client = PusherChannelsClient.websocket(
    options: hostOptions,
    // Connection exceptions are handled here
    connectionErrorHandler: (exception, trace, refresh) async {
      // This method allows you to reconnect if any error is occurred.
      refresh();
    },
  );

  PublicChannel myPublicChannel = client.publicChannel('activate');
  // PrivateChannel myPrivateChannel = client.privateChannel(
  //   'activate',
  //   authorizationDelegate:
  //       EndpointAuthorizableChannelTokenAuthorizationDelegate.forPrivateChannel(
  //     authorizationEndpoint:
  //         Uri.parse('http://127.0.0.1:8000/api/broadcasting/auth'),
  //     headers: {
  //       'Authorization': 'Bearer 32|7XfLVRezeLoIwVvKDopaGyaSfKxRisa7VqvRd0FUcc8c15c2',
  //     },
  //   ),
  // );

  StreamSubscription<ChannelReadEvent> somePublicChannelEventSubs =
      myPublicChannel.bind('App\\Events\\ActivateEvent').listen((event) {
    // myPrivateChannel.trigger(eventName: "App\\Events\\FlutterEvent",data:  {'message': 'Hello Flutter!'});
    print("Nouvel evenement");
    print(event.channel.name);
  });

  // Organizing all subscriptions into 1 for readability
  final allEventSubs = <StreamSubscription?>[
    somePublicChannelEventSubs,
  ];
  // Organizing all channels for readibility
  final allChannels = <Channel>[
    myPublicChannel,
  ];

  final StreamSubscription connectionSubs =
      client.onConnectionEstablished.listen((_) {
    for (final channel in allChannels) {
      // Subscribes to the channel if didn't unsubscribe from it intentionally
      channel.subscribeIfNotUnsubscribed();
    }
  });

  // Connect with the client
  unawaited(client
      .connect()
      .then((value) => print('Socket connected on ws//13.36.119.0')));

 
 
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    connect();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Ylomi'),
        ),
        body: Center(
          child: ElevatedButton(
            onPressed: () {
              // Logique de traitement lors du clic sur le bouton
            },
            child: Text('Click Me'),
          ),
        ),
      ),
    );
  }
}
